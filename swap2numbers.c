#include<stdio.h>

int swap(int*x,int*y);

int main()
{
    int num1,num2;
    printf("enter two numbers\n");
    scanf("%d%d",&num1,&num2);
    printf("the numbers before swapping are %d and %d\n",num1,num2);
    swap(&num1,&num2);
    printf("the numbers after swapping are%d and %d\n",num1,num2);
    return 0;
}

int swap(int*x,int*y)
{
  int t;
  
  t=*x; 
  *x=*y;
  *y=t;
}
